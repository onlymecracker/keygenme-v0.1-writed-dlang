import dlangui;

import crypto.aes;
import crypto.padding;

import std.windows.registry;

import std.base64;
import std.datetime;
import std.conv;
import std.stdio;

mixin APP_ENTRY_POINT;

/// entry point for dlangui based application
extern (C) int UIAppMain(string[] args) {
    // create window
    Window window = Platform.instance.createWindow(
        "KeygenMe - onlymecracker - THT", 
        null,
        0,
        350,
        100
    );

    auto hlayout = new HorizontalLayout();
    hlayout.margins = 20;
    hlayout.padding = 10;

    auto checkBtn = (new Button("chkBtn", "Kontrol"d)).textColor(0xFF0000).fontSize(24);
    auto keygen = new EditLine("keygen", ""d);

    keygen.fontSize = 24;
    keygen.minWidth = 350;
    keygen.minHeight = 52;

    // string key = "12341234123412341234123412341234";
    // ubyte[] message = cast(ubyte[])"123412341234123412341234123412341";
    // ubyte[] iv = [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 1, 2, 3, 4];

    // ubyte[] buffer = AESUtils.encrypt!AES128(message, key, iv, PaddingMode.PKCS5);
    // buffer = AESUtils.decrypt!AES128(buffer, key, iv, PaddingMode.PKCS5);

    // assert(message == buffer);

    auto regKey = Registry.currentUser()
        .getKey("Software")
        .getKey("Microsoft")
        .getKey("Windows")
        .getKey("CurrentVersion")
        .getKey("Run", REGSAM.KEY_ALL_ACCESS);




    auto static_key_ = "Z8KN27U6XCR0906STGQ0YDGSK0ETVAJK";
    regKey.setValue("THT_PUBLIC_KEY", static_key_);

    auto st1 = Clock.currTime();
    auto static_key =  to!string(st1.hour) ~ "Z8KN2-7U6XCR0906STGQ0YDGSK0E-TVAJK" ~ to!string(st1.hour);
    auto convert = cast(ubyte[])static_key;

    const(char)[] encoded = Base64.encode(convert[]);


    checkBtn.click = delegate(Widget src) {
        if (to!string(keygen.text) == to!string(encoded)){
            window.showMessageBox("Tebriko"d, "Eline sağlık, bu kadardı.");
        }
        // window.showMessageBox("Button btn1 pressed"d, "Editor content:"d ~ encrypt_data);
        return true;
    };


    hlayout.addChild(keygen);
	hlayout.addChild(checkBtn);

    // create some widget to show in window
    window.mainWidget = hlayout;

    // show window
    window.show();

    // run message loop
    return Platform.instance.enterMessageLoop();
}